# Information / Информация

SPEC-файл для создания RPM-пакета **meta-users**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/meta`.
2. Установить пакет: `dnf install meta-users`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)