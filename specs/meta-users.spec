%global d_bin                   %{_bindir}
%global d_home                  /home

%global d_storage_00            %{d_home}/storage_00
%global d_app                   %{d_storage_00}/app
%global d_web                   %{d_storage_00}/web

%global d_storage_01            %{d_home}/storage_01
%global d_data_03               %{d_storage_01}/data_03
%global d_data_04               %{d_storage_01}/data_04

Name:                           meta-users
Version:                        1.0.1
Release:                        1%{?dist}
Summary:                        META-package for install users
License:                        GPLv3

Requires:                       meta-system

%description
META-package for install users.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%pre
# User: app-0000-0009.
for i in {0..9}; do
  getent group "app-000${i}" > /dev/null || groupadd "app-000${i}"
  getent passwd "app-000${i}" > /dev/null || \
    useradd -g "app-000${i}" -d "%{d_app}/app-000${i}" -s /bin/zsh \
    -c "APP_000${i}" "app-000${i}"
done

# User: app-0010-0019.
for i in {0..9}; do
  getent group "app-001${i}" > /dev/null || groupadd "app-001${i}"
  getent passwd "app-001${i}" > /dev/null || \
    useradd -g "app-001${i}" -d "%{d_app}/app-001${i}" -s /bin/zsh \
    -c "APP_001${i}" "app-001${i}"
done

# User: app-0020-0029.
for i in {0..9}; do
  getent group "app-002${i}" > /dev/null || groupadd "app-002${i}"
  getent passwd "app-002${i}" > /dev/null || \
    useradd -g "app-002${i}" -d "%{d_app}/app-002${i}" -s /bin/zsh \
    -c "APP_002${i}" "app-002${i}"
done

# User: web-0000-0009.
for i in {0..9}; do
  getent group "web-000${i}" > /dev/null || groupadd "web-000${i}"
  getent passwd "web-000${i}" > /dev/null || \
    useradd -g "web-000${i}" -d "%{d_web}/web-000${i}" -s /bin/zsh \
    -c "WEB_000${i}" "web-000${i}"
done

# User: web-0010-0019.
for i in {0..9}; do
  getent group "web-001${i}" > /dev/null || groupadd "web-001${i}"
  getent passwd "web-001${i}" > /dev/null || \
    useradd -g "web-001${i}" -d "%{d_web}/web-001${i}" -s /bin/zsh \
    -c "WEB_001${i}" "web-001${i}"
done

# User: web-0020-0029.
for i in {0..9}; do
  getent group "web-002${i}" > /dev/null || groupadd "web-002${i}"
  getent passwd "web-002${i}" > /dev/null || \
    useradd -g "web-002${i}" -d "%{d_web}/web-002${i}" -s /bin/zsh \
    -c "WEB_002${i}" "web-002${i}"
done

exit 0


%install
%{__rm} -rf %{buildroot}

# Directory: app-000*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_03}/app-000${i}
done

# Directory: app-001*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_03}/app-001${i}
done

# Directory: app-002*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_03}/app-002${i}
done

# Directory: web-000*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_04}/web-000${i}
done

# Directory: web-001*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_04}/web-001${i}
done

# Directory: web-002*.
for i in {0..9}; do
  %{__install} -dp -m 0755 %{buildroot}%{d_data_04}/web-002${i}
done


%files
# Directory: data_03/app-*.
%attr(0700,app-0000,app-0000) %dir %{d_data_03}/app-0000
%attr(0700,app-0001,app-0001) %dir %{d_data_03}/app-0001
%attr(0700,app-0002,app-0002) %dir %{d_data_03}/app-0002
%attr(0700,app-0003,app-0003) %dir %{d_data_03}/app-0003
%attr(0700,app-0004,app-0004) %dir %{d_data_03}/app-0004
%attr(0700,app-0005,app-0005) %dir %{d_data_03}/app-0005
%attr(0700,app-0006,app-0006) %dir %{d_data_03}/app-0006
%attr(0700,app-0007,app-0007) %dir %{d_data_03}/app-0007
%attr(0700,app-0008,app-0008) %dir %{d_data_03}/app-0008
%attr(0700,app-0009,app-0009) %dir %{d_data_03}/app-0009
%attr(0700,app-0010,app-0010) %dir %{d_data_03}/app-0010
%attr(0700,app-0011,app-0011) %dir %{d_data_03}/app-0011
%attr(0700,app-0012,app-0012) %dir %{d_data_03}/app-0012
%attr(0700,app-0013,app-0013) %dir %{d_data_03}/app-0013
%attr(0700,app-0014,app-0014) %dir %{d_data_03}/app-0014
%attr(0700,app-0015,app-0015) %dir %{d_data_03}/app-0015
%attr(0700,app-0016,app-0016) %dir %{d_data_03}/app-0016
%attr(0700,app-0017,app-0017) %dir %{d_data_03}/app-0017
%attr(0700,app-0018,app-0018) %dir %{d_data_03}/app-0018
%attr(0700,app-0019,app-0019) %dir %{d_data_03}/app-0019
%attr(0700,app-0020,app-0020) %dir %{d_data_03}/app-0020
%attr(0700,app-0021,app-0021) %dir %{d_data_03}/app-0021
%attr(0700,app-0022,app-0022) %dir %{d_data_03}/app-0022
%attr(0700,app-0023,app-0023) %dir %{d_data_03}/app-0023
%attr(0700,app-0024,app-0024) %dir %{d_data_03}/app-0024
%attr(0700,app-0025,app-0025) %dir %{d_data_03}/app-0025
%attr(0700,app-0026,app-0026) %dir %{d_data_03}/app-0026
%attr(0700,app-0027,app-0027) %dir %{d_data_03}/app-0027
%attr(0700,app-0028,app-0028) %dir %{d_data_03}/app-0028
%attr(0700,app-0029,app-0029) %dir %{d_data_03}/app-0029

# Directory: data_04/web-*.
%attr(0700,web-0000,web-0000) %dir %{d_data_04}/web-0000
%attr(0700,web-0001,web-0001) %dir %{d_data_04}/web-0001
%attr(0700,web-0002,web-0002) %dir %{d_data_04}/web-0002
%attr(0700,web-0003,web-0003) %dir %{d_data_04}/web-0003
%attr(0700,web-0004,web-0004) %dir %{d_data_04}/web-0004
%attr(0700,web-0005,web-0005) %dir %{d_data_04}/web-0005
%attr(0700,web-0006,web-0006) %dir %{d_data_04}/web-0006
%attr(0700,web-0007,web-0007) %dir %{d_data_04}/web-0007
%attr(0700,web-0008,web-0008) %dir %{d_data_04}/web-0008
%attr(0700,web-0009,web-0009) %dir %{d_data_04}/web-0009
%attr(0700,web-0010,web-0010) %dir %{d_data_04}/web-0010
%attr(0700,web-0011,web-0011) %dir %{d_data_04}/web-0011
%attr(0700,web-0012,web-0012) %dir %{d_data_04}/web-0012
%attr(0700,web-0013,web-0013) %dir %{d_data_04}/web-0013
%attr(0700,web-0014,web-0014) %dir %{d_data_04}/web-0014
%attr(0700,web-0015,web-0015) %dir %{d_data_04}/web-0015
%attr(0700,web-0016,web-0016) %dir %{d_data_04}/web-0016
%attr(0700,web-0017,web-0017) %dir %{d_data_04}/web-0017
%attr(0700,web-0018,web-0018) %dir %{d_data_04}/web-0018
%attr(0700,web-0019,web-0019) %dir %{d_data_04}/web-0019
%attr(0700,web-0020,web-0020) %dir %{d_data_04}/web-0020
%attr(0700,web-0021,web-0021) %dir %{d_data_04}/web-0021
%attr(0700,web-0022,web-0022) %dir %{d_data_04}/web-0022
%attr(0700,web-0023,web-0023) %dir %{d_data_04}/web-0023
%attr(0700,web-0024,web-0024) %dir %{d_data_04}/web-0024
%attr(0700,web-0025,web-0025) %dir %{d_data_04}/web-0025
%attr(0700,web-0026,web-0026) %dir %{d_data_04}/web-0026
%attr(0700,web-0027,web-0027) %dir %{d_data_04}/web-0027
%attr(0700,web-0028,web-0028) %dir %{d_data_04}/web-0028
%attr(0700,web-0029,web-0029) %dir %{d_data_04}/web-0029


%changelog
* Sun Nov 03 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-1
- UPD: Change user names.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: SPEC-file.

* Sun Jul 28 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-6
- UPD: SPEC-file.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-5
- UPD: SPEC-file.

* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- UPD: SPEC-file.

* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-3
- UPD: SPEC-file.

* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- FIX: "useradd: invalid comment".

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
